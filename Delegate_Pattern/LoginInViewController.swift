//
//  LoginInViewController.swift
//  Delegate_Pattern
//
//  Created by  drake on 2020/05/17.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

protocol LoginIn {
    func ChangeInput(name: String)
}

class LoginInViewController: UIViewController {
    @IBOutlet weak var NameTextField: UITextField!
    
    var delegate : LoginIn?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func EditingChanged(_ sender: UITextField) {
        delegate?.ChangeInput(name: sender.text ?? "")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
