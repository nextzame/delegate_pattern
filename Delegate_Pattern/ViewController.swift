//
//  ViewController.swift
//  Delegate_Pattern
//
//  Created by  drake on 2020/05/17.
//  Copyright © 2020 drake. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var NameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "moveToLoginVC" {
            let loginVC = segue.destination as! LoginInViewController
            loginVC.delegate = self
        }
    }

}

extension ViewController : LoginIn {
    func ChangeInput(name: String) {
        NameLabel.text = name
    }
}

